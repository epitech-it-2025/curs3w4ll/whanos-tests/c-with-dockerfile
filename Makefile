##
## EPITECH PROJECT, 2021
## Makefile
## File description:
## Makefile | https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html#Implicit-Variables
##

CC			= 	gcc
RM			= 	rm -rf

SRC         =   matchstick.c            \
 				usage.c 				\
				errors/main_error.c 	\
				init_free_map.c 		\
				user_move.c 			\
				ai_move.c 				\

SRC_MAIN 	= 	main.c 	\

DIR_SRC 	= 	app/src/

OBJ 		= 	$(addprefix $(DIR_SRC), $(SRC:.c=.o)) $(addprefix $(DIR_SRC), $(SRC_MAIN:.c=.o))

NAME        =   compiled-app

CFLAGS		=	-Wall -Werror -Wextra
LDFLAGS		= 	-L./app/lib -lmy
CPPFLAGS	=	-I./app/include -I./app/lib/include

all: 		$(NAME)

$(NAME): 	lib $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS) $(CPPFLAGS) $(CFLAGS)

lib:
	make -C app/lib/

.PHONY: all

/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Usage management prototype
*/


#ifndef USAGE_H
#define USAGE_H

#include "macros.h"

ret_t check_usage(int ac, const char **av);

#endif /* USAGE_H */

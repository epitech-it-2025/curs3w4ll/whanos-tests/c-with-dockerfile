/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Macros for Matchstick project
*/


#ifndef MYMACROS_H
#define MYMACROS_H

#include <stddef.h>
#include <stdio.h>

typedef enum {
    MYEXIT_FAILURE = 84,
    MYEXIT_SUCCESS = 0,
    ERROR = 0,
    SUCCESS = 1,
    SPECIFIC = -1,
    USER = 1,
    AI = 2,
    EXIT = 3
} ret_t;

#define DELIM_CHAR '*'
#define FREE_CHAR ' '
#define STICK_CHAR '|'

#define AI_WIN_MSG "You lost, too bad...\n"
#define USER_WIN_MSG "I lost... snif... but I'll get you next time!!\n"

#define ERROR_FD 2
#define ERROR_MALLOC "Error: Malloc failed.\n"
#define ERROR_NBR_ARGS "Error: The program need 2 arguments.\n"
#define ERROR_INVALID_ARGS "Error: The arguments need to be positive numbers.\n"
#define ERROR_NBR_LINES "Error: Number of lines need to be 1 < n < 100.\n"
#define ERROR_NBR_MATCHES "Error: Matches number need to be strictly positiv.\n"
#define ERROR_INVALID_INPUT "Error: invalid input (positive number expected)\n"
#define ERROR_INVALID_LINE "Error: this line is out of range\n"
#define ERROR_TOOLESS_MATCHES "Error: you have to remove at least one match\n"
#define ERROR_TOOMUCH_MATCHES "Error: not enough matches on this line\n"

#endif /* MYMACROS_H */

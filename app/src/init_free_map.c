/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** matchstick
*/

#include <stdlib.h>
#include "mystring.h"
#include "mymath.h"
#include "game_struct.h"
#include "usage.h"
#include "errors.h"
#include "matchstick.h"

void fill_map_line(game_t *game, short i)
{
    short y;
    short free_side = (game->cols - ((i + 1) * 2 - 1)) / 2;

    for (y = 0; y < game->cols; y++) {
        if (y < free_side || y >= game->cols - free_side)
            (game->map)[i][y] = FREE_CHAR;
        else
            (game->map)[i][y] = STICK_CHAR;
    }
    (game->map)[i][y] = '\0';
}

ret_t init_gamestruct(const char **av, game_t *game)
{
    short i;

    game->lines = my_getnbr(av[1]);
    game->cols = game->lines * 2 - 1;
    game->map = malloc(sizeof(char *) * (game->lines + 1));
    if (!game->map)
        return my_print_int_error(ERROR_MALLOC, ERROR_FD, ERROR);
    for (i = 0; i < game->lines; i++) {
        (game->map)[i] = malloc(sizeof(char) * game->cols + 1);
        if (!(game->map)[i])
            return my_print_int_error(ERROR_MALLOC, ERROR_FD, ERROR);
        fill_map_line(game, i);
    }
    (game->map)[i] = NULL;
    game->matches = my_getnbr(av[2]);
    return SUCCESS;
}

void free_map(char **map)
{
    for (int i = 0; map[i]; i++)
        free(map[i]);
    free(map);
}

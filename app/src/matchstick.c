/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** matchstick
*/

#include <stdlib.h>
#include "mystring.h"
#include "mymath.h"
#include "usage.h"
#include "errors.h"
#include "matchstick.h"

void display_map(short cols, const char **map)
{
    for (int i = 0; i < cols + 2; i++)
        my_putchar(DELIM_CHAR);
    my_putchar('\n');
    for (int i = 0; map[i]; i++) {
        my_putchar(DELIM_CHAR);
        my_putstr(map[i]);
        my_putstr("*\n");
    }
    for (int i = 0; i < cols + 2; i++)
        my_putchar(DELIM_CHAR);
    my_putchar('\n');
}

ret_t check_endgame(const char **map, ret_t player)
{
    if (player == EXIT)
        return MYEXIT_SUCCESS;
    for (int i = 0; map[i]; i++) {
        for (int y = 0; map[i][y]; y++) {
            if (map[i][y] == STICK_CHAR)
                return SPECIFIC;
        }
    }
    return player;
}

ret_t launch_game(game_t game)
{
    ret_t ret = SPECIFIC;

    while (ret == SPECIFIC) {
        display_map(game.cols, (const char **)game.map);
        if (game.cur_player == USER)
            get_player_reads(&game);
        else
            do_ai_move(&game);
        ret = check_endgame((const char **)game.map, game.cur_player);
    }
    return ret;
}

void do_remove(char **map_line, short line, short matches)
{
    int i = my_strlen(*map_line) - 1;

    my_putstr(" removed ");
    my_put_nbr(matches);
    my_putstr(" match(es) from line ");
    my_put_nbr(line);
    my_putchar('\n');
    while (i >= 0 && (*map_line)[i] != STICK_CHAR)
        i--;
    while (i >= 0 && matches) {
        (*map_line)[i] = FREE_CHAR;
        i--;
        matches--;
    }
}

ret_t matchstick(int ac, const char **av)
{
    game_t game;
    ret_t ret;

    if (!check_usage(ac, av))
        return MYEXIT_SUCCESS;
    if (!check_errors(ac, av))
        return MYEXIT_FAILURE;
    if (!init_gamestruct(av, &game))
        return MYEXIT_FAILURE;
    game.cur_player = USER;
    ret = launch_game(game);
    if (ret != MYEXIT_SUCCESS) {
        display_map(game.cols, (const char **)game.map);
        if (ret == AI)
            my_putstr(AI_WIN_MSG);
        else
            my_putstr(USER_WIN_MSG);
    }
    free_map(game.map);
    return ret;
}

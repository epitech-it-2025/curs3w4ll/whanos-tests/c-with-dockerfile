/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Usage management
*/

#include "macros.h"
#include "mystring.h"

ret_t print_usage(void)
{
    my_putstr("USAGE\n"
    "    ./matchstick [lines_nbr] [max_substitution]\n"
    "DESCRIPTION\n"
    "    lines_nbr: The number of lines the generated layout will have\n"
    "    max_substitution: The maximal number of "
    "sticks you can remove on one line\n");
    return ERROR;
}

ret_t check_usage(int ac, const char **av)
{
    for (int i = 1; i < ac; i++)
        if (!my_strcmp(av[i], "-h") || !my_strcmp(av[i], "--help"))
            return (print_usage());
    return SUCCESS;
}

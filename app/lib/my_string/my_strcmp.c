/*
** EPITECH PROJECT, 2020
** My_strcmp
** File description:
** Compare two strings
*/

#include "mystring.h"

int my_strcmp(const char *str1, const char *str2)
{
    int len;

    len = my_strlen(str1);
    if (len != my_strlen(str2))
        return 1;
    for (int i = 0; i <= len; i++)
        if (str1[i] != str2[i])
            return 1;
    return 0;
}

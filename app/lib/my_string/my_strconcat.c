/*
** EPITECH PROJECT, 2020
** My_strconcat
** File description:
** Concatenate two string and create new malloc
*/

#include <stdlib.h>
#include "mystring.h"

char *my_strconcat(const char *str1, char const *str2)
{
    int total_len = my_strlen(str1) + my_strlen(str2);
    char *result = malloc(sizeof(char) * (total_len + 1));
    int pos = 0;

    if (!result)
        return NULL;
    for (int i = 0; str1[i]; i++, pos++)
        result[pos] = str1[i];
    for (int i = 0; str2[i]; i++, pos++)
        result[pos] = str2[i];
    result[pos] = '\0';
    return result;
}

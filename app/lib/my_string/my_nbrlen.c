/*
** EPITECH PROJECT, 2020
** my_nbrlen
** File description:
** Return the len of a number
*/

int my_nbrlen(int nbr)
{
    int len = 1;

    if (nbr < 0)
        len++;
    while (nbr > 9 || nbr < -9) {
        nbr /= 10;
        len++;
    }
    return len;
}
